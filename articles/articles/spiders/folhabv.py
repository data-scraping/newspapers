# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from articles.items import Article


class FolhabvSpider(scrapy.Spider):
    name = 'folhabv'
    start_urls = ['https://www.folhabv.com.br/lista/noticia/pagina/%s/' % page for page in range(1,1000)]

    def parse(self, response):
        urls = response.css('.titulo-img>a::attr(href)').extract()
        
        for url in urls:
            full_url = 'https://www.folhabv.com.br/{0}'.format(url)
            yield scrapy.Request(full_url, callback=self.parse_page)            

    def parse_page(self, response):
        thumbs_up = ''.join([str(text).replace('\r\n', '').strip() for text in
                             response.xpath("//div[@id='comment-noticia']/a[@title='Aprovar']/text()").extract()])
        thumbs_down = ''.join([str(text).replace('\r\n', '').strip() for text in
                               response.xpath("//div[@id='comment-noticia']/a[@title='Reprovar']/text()").extract()])

        article = Article({
            'url': response.url,
            'title': str(response.css('.title::text').extract_first()).strip(),
            'content': ''.join([str(p).strip().replace('\n', ' ') for p in response.css('.text>p::text').extract()]),
            'author': str(response.css('.jornalista>a::text').extract_first()).strip(),
            'category': ''.join([str(p).replace('\\n', '').replace('\\r', '').strip() for p in
                                 response.css('.p-chave::text').extract()]),
            'published_on': datetime.strptime(str(response.css('.date::text').extract_first()).strip(),
                                              'Em %d/%m/%Y às %H:%M'),
            'thumbs_up': int(thumbs_up[thumbs_up.find('(') + 1:thumbs_up.find(')')]),
            'thumbs_down': int(thumbs_down[thumbs_down.find('(') + 1:thumbs_down.find(')')])
        })

        return article
