# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import os
import pymongo
import logging
from urllib.parse import quote_plus
from dotenv import load_dotenv

class MongoDBPipeline(object):

    def __init__(self):
        dotenv_file = os.path.abspath(os.path.join(os.path.dirname(os.getcwd()), '.env'))
        load_dotenv(dotenv_file)

        MONGO_ROOT_USERNAME = quote_plus(os.environ.get('MONGO_ROOT_USERNAME'))
        MONGO_ROOT_PASSWORD = quote_plus(os.environ.get('MONGO_ROOT_PASSWORD'))
        MONGO_HOST = quote_plus(os.environ.get('MONGO_HOST'))
        APP_MONGO_DB = quote_plus(os.environ.get('APP_MONGO_DB'))

        client = pymongo.MongoClient('mongodb://%s:%s@%s' % (MONGO_ROOT_USERNAME, MONGO_ROOT_PASSWORD, MONGO_HOST))

        db = client[APP_MONGO_DB]
        self.collection = db.articles

    def process_item(self, item, spider):
        if self.collection.find_one({'url': item['url']}) is None:
            self.collection.insert(dict(item))
            logging.log(logging.INFO, "New article added to database!")
        else:
            logging.log(logging.INFO, "Article already exists in database!")

        return item
