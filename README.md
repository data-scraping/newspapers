# Newspapers

Scraping newspapers data.

## Install

The project requires python >= 3.6 and the following python libraries:

- [scrapy](https://scrapy.org)
- [jupyter](http://jupyter.org)
- [pymongo](http://api.mongodb.com/python/current/tutorial.html)
- [dotenv](https://github.com/theskumar/python-dotenv)

Feel free to install using the `requirements.txt` file.

## Start the `mongodb` and `mongo_express` containers
Berofe runing the docker-compose command, rename the .env.example to .env and replace `[change here]` to your own credentials.

To create the mongodb and mongo-express containers, navigate to the top level project directory `newspapers/` and run the following command:
```console
$ docker-compose up -d
```
To access mongo-express you can hit http://localhost:8081 in your browser.

## Running the ["spiders"](https://docs.scrapy.org/en/latest/topics/spiders.html)
In a terminal, navigate to the `articles/` directory and for each of the following URLs, execute the respective command `scrapy crawl ...`:

1) [https://folhabv.com.br](https://folhabv.com.br)
```console
$ scrapy crawl folhabv
```

## Running jupyter

In a terminal or command window, navigate to the top level project directory `newspapers/` and run the following command:
```console
$ jupyter notebook articles/collect.ipynb
```
This will open the iPython Notebook software and project file in your browser.